unit UVK_LVPatterns;
{
Implemented item types list:
  data
  text
  rect
  ellipse
  image
  accessory

JSON fields availabe:

Common properties
    Kind: string;
    Name: string;
    Place: TPlaceExpressions;
      X, Y, W, H: string;
      //mutable='XYWH' means all fields are mutable and need to be evaluated EVERY time
      // oterwise ('') all of them are cached
      mutable:string;
      // default priority is 'XYWH' but we can change it for example if W depends of X
      priority:string;
    HAlign: TListItemAlign;
    VAlign: TListItemAlign; //0,1,2 - Leading, Center, Trailing
    Visible:boolean;

Text properties
    WordWrap:boolean;
    Color:String;
    TextHAlign: TTextAlign;
    TextVAlign: TTextAlign;  // 0,1,2 - Center, Leading, Trailing !!! DIFFERENT TO HAlign/VAlign !!!
    Font:TVK_FontDescription;
      Size:Single;
      Style:string;

Rect properties
    Color:string;
    BorderColor:string;
    LineWidth:integer;

Ellipse properties
    Color:string;
    BorderColor:string;
    LineWidth:integer;
    Circle:boolean; // set proportion width/height to 1

Accessory
    AccType:string // 'More' | 'Check' | 'Detail'
}
interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  System.generics.collections, FMX.Types, FMX.Graphics, FMX.Controls, FMX.TextLayout,
  FMX.ListView.Types, FMX.ListView.Appearances, FMX.ListView.Adapters.Base, FMX.ListView,
  JsonableObject, XSuperObject, UVK_ExpressionParser, UVK_LVListItemElements;

type
  TVK_LVPattern = class;

  TVK_LV_Variable = class
  public
    expression:string;
    value:Single;
    mutable: Boolean;
    evaluated: Boolean; // if false then needs to be evaluated from expression
  end;

  TVK_LV_Variables = class(TObjectDictionary<string,TVK_LV_Variable>)
  public
    Pattern: TVK_LVPattern;
    function ValueOf(const VarName:string):single;
  end;

  TPlaceExpressions = record
    X, Y, W, H: string;
    //mutable='XYWH' means all fields are mutable and need to be evaluated EVERY time
    // oterwise ('') all of them are cached
    mutable:string;
    priority:string; //
  end;

  TVK_LVObject = class
  private
    FX: single;
    FXset:boolean;
    FY: single;
    FYset:boolean;
    FW: single;
    FWset:boolean;
    FH: single;
    FHset:boolean;
    function GetValue(const AField,AExpr: string): Single;
    function Get_HAlign: string;
    function Get_VAlign: string;
    procedure Set_HAlign(const Value: string);
    procedure Set_VAlign(const Value: string);
  public
    [Weak][DISABLE] Pattern: TVK_LVPattern;

    Kind: string;
    Name: string;
    Place: TPlaceExpressions;

    [DISABLE]
    HAlign: TListItemAlign;
    [DISABLE]
    VAlign: TListItemAlign; //0,1,2 - Leading, Center, Trailing
    Visible:boolean;

    function CalculateWidth(const d: TListItemDrawable): single; virtual;
    function CalculateHeight(const d: TListItemDrawable): single; virtual;
    function CreateDrawable(const AItem: TlistViewItem): TListItemDrawable; virtual;
    function Drawable(const AItem: TlistViewItem): TListItemDrawable;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); virtual;

    constructor Create; virtual;
    procedure ResetPlace;

    [DISABLE]
    property X: single read FX;
    [DISABLE]
    property Y: single read FY;
    [DISABLE]
    property W: single read FW;
    [DISABLE]
    property H: single read FH;
    [Alias('HAlign')]
    property _HAlign:string read Get_HAlign write Set_HAlign;
    [Alias('VAlign')]
    property _VAlign:string read Get_VAlign write Set_VAlign;
  end;

  TVK_FontDescription=record
    Size:string;
    Style:string;
  end;
  TVK_LVTextObject = class(TVK_LVObject)
  private
    procedure SetupFont(const t: TListItemText);
    function GetTextHAlign: string;
    function GetTextVAlign: string;
    procedure SetTextHAlign(const Value: string);
    procedure SetTextVAlign(const Value: string);
  public
    [DISABLE]
    // Pattern object set this during object creation
    // so we doesn't need to create it here
    FTextLayout: TTextLayout;

    WordWrap:boolean;
    Color:String;
    Font:TVK_FontDescription;
    [DISABLE]
    TextHAlign: TTextAlign;
    [DISABLE]
    TextVAlign: TTextAlign;  // 0,1,2 - Center, Leading, Trailing
    Text:string;

    constructor Create; override;

    function CalculateWidth(const d: TListItemDrawable): single; override;
    function CalculateHeight(const d: TListItemDrawable): single; override;
    function CreateDrawable(const AItem: TListViewItem): TListItemDrawable; override;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); override;
    [Alias('TextHAlign')]
    property _TextHAlign:string read GetTextHAlign write SetTextHAlign;
    [Alias('TextVAlign')]
    property _TextVAlign:string read GetTextVAlign write SetTextVAlign;
  end;

  TVK_LVImageObject = class(TVK_LVObject)
  private
    function GetScalingMode: string;
    procedure SetScalingMode(const Value: string);
  public
    [DISABLE]
    ScalingMode : TImageScalingMode;// = (StretchWithAspect, Original, Stretch);

    function CreateDrawable(const AItem: TListViewItem): TListItemDrawable; override;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); override;

    [Alias('ScalingMode')]
    property _ScalingMode: string read GetScalingMode write SetScalingMode;
  end;


  TVK_LVRectObject = class(TVK_LVObject)
  public
    Color:string;
    BorderColor:string;
    LineWidth:integer;
    SelectedColor:string;
    SelectedBorderColor:string;
    XRadius, YRadius: integer;
    Shadow:TVKShadowProps;
    function CreateDrawable(const AItem: TListViewItem): TListItemDrawable; override;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); override;
  end;

  TVK_LVEllipseObject = class(TVK_LVObject)
  public
    Color:string;
    BorderColor:string;
    LineWidth:integer;
    Circle:Boolean;
    function CreateDrawable(const AItem: TListViewItem): TListItemDrawable; override;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); override;
  end;

  TVK_LVAccessoryObject = class(TVK_LVObject)
  private
    function Get_AccType: string;
    procedure Set_AccType(const Value: string);
  public
    [DISABLE]
    AccType:TAccessoryType;
    function CreateDrawable(const AItem: TListViewItem): TListItemDrawable; override;
    procedure SetupDrawable(const ADrawable: TListItemDrawable); override;
    [alias('AccType')]
    property _AccType:string read Get_AccType write Set_AccType;
  end;


  TVK_LVObjects = class(TObjectList<TVK_LVObject>)
  public
    function IndexOfName(const AName: string):Integer;
    function ByName(const AName:string):TVK_LVObject;
    procedure ResetPlaces();
  end;


  TVK_LVColumn = class
  private
    FWidth: Single;
  public
    Weight: Integer;
    [DISABLE]
    property Width: Single read FWidth;
  end;

  TVK_LVColumns = class(TObjectList<TVK_LVColumn>)
  end;


  TNowParsing=record
    [weak] Obj:TVK_LVObject; // NIL - ��� ��� ListViewItem
    [weak] Item:TListViewItem; // ���� � ��� NIL, �� ���� � ListView
    Field:string;
    procedure Clear;
    procedure Setup(const AObj:TVK_LVObject; const AItem:TListViewItem; const AField:string);
  end;

  TVK_LVOnGetValueEvent = procedure (Sender:TObject; ANowParsing:TNowParsing; const AVarName:string; var Result:single) of object;

  TVK_LVPattern = class(TJsonableObject)
  private
    FNowParsing:TNowParsing;
    FOnGetValue: TVK_LVOnGetValueEvent;
    procedure LoadObjects(X: ISuperObject);
    procedure LoadVariables(X: ISuperObject);
  public
    [DISABLE]
    LV: TListView;
    [DISABLE]
    Parser: TVK_ExpressionParser;
    [DISABLE]
    TextLayout: TTextLayout;

    [DISABLE]
    Columns: TVK_LVColumns;
    [Alias('Columns')]
    _Columns: TArray<TVK_LVColumn>;
    [DISABLE]
    Variables: TVK_LV_Variables;

    [DISABLE] // �.�. ������� �������� �����������, ���������� � ������ ����� ��������� �����
    Objects: TVK_LVObjects;

    ItemHeight: string;
    ItemSpaces: TPlaceExpressions;
    SideSpace: string;

    procedure CreateDrawables(const ALVItem: TListViewItem);
    procedure DoLayout(const ALVItem: TListViewItem);
    function GetClientWidth(): Single;
    function GetValue(const AExpr: string): Single; virtual;
    function ExpressionGetValueHandler(Sender:TObject; const AVarName:string): Single;
    procedure SetupListView(const LV: TListView); // �������� �� JSON ��������� � LV

    procedure BeforeSave(X: ISuperObject); override;
    procedure AfterLoad(X: ISuperObject); override;

    constructor Create; overload; override;
    constructor Create(const AListView: TListView); reintroduce; overload;
    destructor Destroy; override;

    property OnGetValue:TVK_LVOnGetValueEvent read FOnGetValue write FOnGetValue;
  end;

  TVK_LVPatterns=class( TObjectList<TVK_LVPattern> )
  public
    function AddFromFile(const Filename :string): TVK_LVPattern;
    function AddFromJSON(const JSON:string): TVK_LVPattern;
  end;

  TListViewItemEvent = procedure(Sender:TObject; const AListViewItem:TListViewItem) of object;
  TVK_LV_BaseAdapter<TDataItem: class> = class
  private
    FOnAfterLayout: TListViewItemEvent;
  protected
    FUpdating:integer;
  public
    LV: TListView;
    Patterns: TVK_LVPatterns;
    // �������� ���� ������ ListView
    procedure ResetView(); virtual;
    // ���������� ItemSpaces � ������ ����� ��� ����� ListView ����
    procedure SetupListView(); virtual;
    // ���������� ��� ������� ListViewItem
    // ������������� �������� ��� ���� ��������� � ���
    procedure SetupContent(const AListViewItem: TListViewItem); virtual;
    // ����� �������� ��� ���������� ListViewItem
    // �� ��������� ������ ���������� ������
    function SelectPattern(const AListViewItem: TListViewItem): TVK_LVPattern; virtual;
    // ���������� �������� � ��������� Drawable ���������
    procedure DoLayout(const AListViewItem: TListViewItem); virtual;
    // ����� ����� ����������� �������� ����, ��������,
    // ������������ �������� � ����������� �� �������� ����� ������
    procedure AfterLayout(const AListViewItem: TListViewItem); virtual;
    // ���������� ��� OnUpdatingObjects
    procedure OnUpdatingObjects(const Sender: TObject;
        const AItem: TListViewItem; var AHandled: Boolean);

    function AddPatternFromFile(const Filename:string):TVK_LVPattern;
    function AddPatternFromJSON(const JSON:string):TVK_LVPattern;

    procedure ResetPlaces();

    procedure Clear; virtual;

    // Attention!!! The constructor sets the ListView's OnUpdatingObjects event to his own handler!
    constructor Create(const AListView: TListView); overload; virtual;
    constructor Create(const AListView: TListView; const JSON:string); overload; virtual;
    destructor Destroy; override;

    property OnAfterLayout: TListViewItemEvent read FOnAfterLayout write FOnAfterLayout;
  end;

  TVK_LV_FilterEvent = procedure (AData:TObject; var Allowed:boolean) of object;

  TVK_LV_ListAdapter<TDataItem: class> = class(TVK_LV_BaseAdapter<TDataItem>)
  private
    FOnFilter: TVK_LV_FilterEvent;
  public
    function List: TObjectList<TDataItem>; virtual; abstract;
    // ������� ����� ListView ��� ������� �������� �� Data
    procedure ResetView; override;
    procedure ResetItem(const AListViewItem: TListViewItem);
    // ���������� ��� ������� ListViewItem
    // ������ �������� SetItemData ��� SetupDrawableContent
    // ��� ������� �������� �������
    procedure SetupContent(const AListViewItem: TListViewItem); override;
    procedure ResetLayout; virtual;
    procedure Clear; override;

    // ���� ����� �������������� � ����������, ����� ��������� ListViewItem.Data[xxx]
    procedure SetItemData(const AListViewItem: TListViewItem; const AData: TDataItem; const AName:string); virtual;
    // ���� ����� �������������� � ����������, ����� ��������� ������� ListViewItem.Drawable[xxx]
    procedure SetupDrawableContent(const AListViewItem: TListViewItem; const ADrawable: TListItemDrawable; const AData: TDataItem); virtual;
    function GetDataByListItem(const AListViewItem: TListViewItem):TDataItem;
    property OnFilter: TVK_LV_FilterEvent read FOnFilter write FOnFilter;
  end;

  TVK_LV_DataList<T:class>=class(TJsonableObject)
  private
  public
    [DISABLE]
    Items: TObjectList<T>;
    [Alias('Items')]
    _Items: TArray<T>;

    constructor Create; override;
    destructor Destroy; override;

    procedure AfterLoad(X:ISuperObject); override;
  end;


implementation

uses
  system.uiconsts;

resourcestring
  sFIT = 'fit';
  sORIG = 'orig';
  sSTRETCH = 'stretch';

const
  sCENTER = 'center';
  sLEADING = 'leading';
  sTRAILING = 'trailing';
  sMUTABLE = 'mutable';

var
  localFS: TFormatSettings;

{ TVK_LVPattern }


function TVK_LVObject.CalculateHeight(const d: TListItemDrawable): single;
begin
  Result := h;
end;

function TVK_LVObject.CalculateWidth(const d: TListItemDrawable): single;
begin
  Result := w;
end;

constructor TVK_LVObject.Create;
begin
  HAlign := TListItemAlign.Leading;
  VAlign := TListItemAlign.Leading;
  Visible := true;
end;

function TVK_LVObject.CreateDrawable(
  const AItem: TlistViewItem): TListItemDrawable;
begin
  // empty body to avoid compiler warnings when this class is used in de/serialising by XSO
  result := nil;
end;

function TVK_LVObject.Drawable(const AItem: TlistViewItem): TListItemDrawable;
begin
  if SameText( Kind, 'data') then
    exit(nil);

  result := AItem.Objects.FindDrawable(name);
  if result = NIL then
    result := CreateDrawable(AItem);
end;

function TVK_LVTextObject.CalculateHeight(const d: TListItemDrawable): single;
var
  t: TListItemText absolute d;
begin
  FTextLayout.BeginUpdate;
  try
    SetupFont(t);
    // �������������� ��������� ���� ��� ����������� ����������
    // �������� ������������� ������
    FTextLayout.Text := t.Text;
    FTextLayout.MaxSize := TPointF.Create(W, 10000);
    FTextLayout.WordWrap := t.WordWrap;
    FTextLayout.Font := t.Font;
    FTextLayout.HorizontalAlign := t.TextAlign;
    FTextLayout.VerticalAlign := t.TextVertAlign;
  finally
    FTextLayout.EndUpdate;
  end;
  // �������� ������ ������ ��� ������� ����������
  result := FTextLayout.Height;
end;

function TVK_LVTextObject.CalculateWidth(const d: TListItemDrawable): single;
var
  t: TListItemText absolute d;
begin
  FTextLayout.BeginUpdate;
  try
    SetupFont(t);
    // �������������� ��������� ���� ��� ����������� ����������
    // �������� ������������� ������
    FTextLayout.Text := t.Text;
    FTextLayout.WordWrap := t.WordWrap;
    FTextLayout.Font := t.Font;
    FTextLayout.HorizontalAlign := t.TextAlign;
    FTextLayout.VerticalAlign := t.TextVertAlign;

    FTextLayout.MaxSize := TPointF.Create(10000, H);
  finally
    FTextLayout.EndUpdate;
  end;
  // �������� ������ ������ ��� ������� ����������
  result := FTextLayout.Width;
end;

procedure TVK_LVPattern.SetupListView(const LV: TListView);
begin
  FNowParsing.Clear;
  LV.ItemSpaces.Left := GetValue(ItemSpaces.X);
  LV.ItemSpaces.Top := GetValue(ItemSpaces.Y);
  LV.ItemSpaces.Right := GetValue(ItemSpaces.X + ItemSpaces.W);
  LV.ItemSpaces.Bottom := GetValue(ItemSpaces.Y + ItemSpaces.H);
  LV.SideSpace := Round(GetValue(Sidespace));
end;

function TVK_LVObject.GetValue(const AField, AExpr: string): Single;
begin
  Pattern.FNowParsing.Field := AField;
  Result := pattern.GetValue(AExpr);
end;

function TVK_LVObject.Get_HAlign: string;
begin
//0,1,2 - Leading, Center, Trailing
  case HAlign of
    TListItemAlign.Leading: result := sLEADING;
    TListItemAlign.Center: result := sCENTER;
    TListItemAlign.Trailing: result := sTRAILING;
  end;
end;

function TVK_LVObject.Get_VAlign: string;
begin
//0,1,2 - Leading, Center, Trailing
  case VAlign of
    TListItemAlign.Leading: result := sLEADING;
    TListItemAlign.Center: result := sCENTER;
    TListItemAlign.Trailing: result := sTRAILING;
  end;
end;

procedure TVK_LVObject.ResetPlace;
begin
  FXset := false;
  FYset := false;
  FWset := false;
  FHset := false;
end;

procedure TVK_LVObject.SetupDrawable(const ADrawable: TListItemDrawable);
var
  priority:string;
  ch:Char;
  procedure SetField(var F:Single; var FSet:boolean; var Fld:string; FldName:string);
  begin
     if (not FSet) then
    begin
      F:=GetValue(fldName, Fld);
      if not Place.mutable.ToUpper.Contains(FldName) then
        Fset := true;
    end;
  end;

begin
  Pattern.FNowParsing.Obj := Self;
  if Place.priority='' then
    priority := 'XYWH'
  else
  begin
    priority := AnsiUpperCase(Place.priority);
    for ch in 'XYWH' do
    begin
      if not priority.Contains(ch) then
        priority := priority + ch;
    end;
  end;

  for ch in priority do
  begin
    if ch='X' then
      SetField(FX, FXset, place.x, 'X')
    else
    if ch='Y' then
      SetField(FY, FYset, place.y, 'Y')
    else
    if ch='W' then
      SetField(FW, FWset, place.W, 'W')
    else
    if ch='H' then
      SetField(FH, FHset, place.H, 'H')
  end;

//  fx:=GetValue('x', place.x);
//  fy:=GetValue('y', place.y);
//  fw:=GetValue('w', place.W);
//  fh:=GetValue('h', Place.H);
  ADrawable.placeoffset.x := fx;
  ADrawable.placeoffset.y := fy;
  ADrawable.width := fw;
  ADrawable.Height := fh;
  ADrawable.Align := HAlign;
  ADrawable.VertAlign := VAlign;
end;

procedure TVK_LVObject.Set_HAlign(const Value: string);
begin
//0,1,2 - Leading, Center, Trailing
  if AnsiSameText(Value, sCENTER)
  or AnsiSameText(Value, '1')then
    HAlign := TListItemAlign.Center
  else
  if AnsiSameText(Value, sTRAILING)
  or AnsiSameText(Value, '2')then
    HAlign := TListItemAlign.Trailing
  else
    HAlign := TListItemAlign.Leading;
end;

procedure TVK_LVObject.Set_VAlign(const Value: string);
begin
//0,1,2 - Leading, Center, Trailing
  if AnsiSameText(Value, sCENTER)
  or AnsiSameText(Value, '1')then
    VAlign := TListItemAlign.Center
  else
  if AnsiSameText(Value, sTRAILING)
  or AnsiSameText(Value, '2')then
    VAlign := TListItemAlign.Trailing
  else
    VAlign := TListItemAlign.Leading;
end;

constructor TVK_LVTextObject.Create;
begin
  inherited Create;
  TextHAlign := TTextAlign.Leading;
  TextVAlign := TTextAlign.Leading;
end;

function TVK_LVTextObject.CreateDrawable(const AItem: TListViewItem): TListItemDrawable;
begin
  result := TListItemText.Create(AItem);
  Result.Name := Name;
end;


function TVK_LVTextObject.GetTextHAlign: string;
begin
  // 0,1,2 - Center, Leading, Trailing
  case TextHAlign of
    TTextAlign.Center: Result := sCENTER;
    TTextAlign.Leading: Result := sLEADING;
    TTextAlign.Trailing: Result := sTRAILING;
  end;
end;

function TVK_LVTextObject.GetTextVAlign: string;
begin
  // 0,1,2 - Center, Leading, Trailing
  case TextHAlign of
    TTextAlign.Center: Result := sCENTER;
    TTextAlign.Leading: Result := sLEADING;
    TTextAlign.Trailing: Result := sTRAILING;
  end;
end;

procedure TVK_LVTextObject.SetupFont(const t: TListItemText);
var
  fntStyles:TArray<string>;
  i : integer;
begin
  t. Font.Size := GetValue('Font.size', font.Size);
  t.Font.Style := [];
  t.WordWrap := WordWrap;
  fntStyles := font.Style.Split([',']);
  if Color<>'' then
    t.TextColor := stringtoalphacolor(Color);

  for I := 0 to High(fntStyles) do
  begin
    if SameText(fntstyles[i],'bold') then
    begin
      t.Font.Style := t.Font.Style + [TFontStyle.fsBold];
    end;
  end;
end;

procedure TVK_LVTextObject.SetTextHAlign(const Value: string);
begin
  // 0,1,2 - Center, Leading, Trailing
  if AnsiSameText(value, sCENTER)
  or AnsiSameText(value, '0') then
    TextHAlign := ttextalign.Center
  else
  if AnsiSameText(value, sTRAILING)
  or AnsiSameText(Value, '2') then
    TextHAlign := ttextalign.Trailing
  else
    TextHAlign := ttextalign.Leading;
end;

procedure TVK_LVTextObject.SetTextVAlign(const Value: string);
begin
  // 0,1,2 - Center, Leading, Trailing
  if AnsiSameText(value, sCENTER)
  or AnsiSameText(value, '0') then
    TextVAlign := ttextalign.Center
  else
  if AnsiSameText(value, sTRAILING)
  or AnsiSameText(Value, '2') then
    TextVAlign := ttextalign.Trailing
  else
    TextVAlign := ttextalign.Leading;
end;

procedure TVK_LVTextObject.SetupDrawable(const ADrawable: TListItemDrawable);
var
  t: TListItemText absolute ADrawable;
begin
  inherited SetupDrawable(ADrawable);
  t.TextAlign := TextHAlign;
  t.TextVertAlign := TextVAlign;
  if t.Text='' then
    t.Text := text;
  SetupFont(t);
end;

procedure TVK_LVPattern.LoadObjects(X: ISuperObject);
var
  arrItem: ISuperObject;
  arr: ISuperArray;
  I: Integer;
  kind: string;
  obj: TVK_LVObject;
  s:string;
begin
  ArrayToList<TVK_LVColumn>(_Columns,Columns);
  arr := X.A['Objects'];
  for I := 0 to arr.Length-1 do
  begin
    obj := nil;
    arrItem := arr.O[i];
    kind := arrItem.S['kind'];
    if SameText(kind, 'data') then
    begin
      s:=arrItem.asjson();
      obj := TVK_LVObject.FromJSON(s)
    end
    else if SameText(kind, 'text') then
    begin
      obj := TVK_LVTextObject.FromJSON(arrItem);
      (obj as TVK_LVTextObject).FTextLayout := TextLayout;
    end
    else if SameText(kind, 'rect') then
    begin
      obj := TVK_LVRectObject.FromJSON(arrItem);
    end
    else if SameText(kind, 'ellipse') then
    begin
      obj := TVK_LVEllipseObject.FromJSON(arrItem);
    end
    else if SameText(kind, 'image') then
    begin
      obj := TVK_LVImageObject.FromJSON(arrItem);
    end
    else if SameText(kind, 'accessory') then
    begin
      obj := TVK_LVAccessoryObject.FromJSON(arrItem);
    end;
    if obj<>NIL then
    begin
      obj.Pattern := self;
      objects.Add(obj);
    end;
  end;
end;

procedure TVK_LVPattern.LoadVariables(X: ISuperObject);
var
  arrItem: ISuperObject;
  arr: ISuperArray;
  I: Integer;
  v: TVK_LV_Variable;
  key:string;
begin
  arr := X.A['Variables'];
  for I := 0 to arr.Length-1 do
  begin
    arrItem := arr.o[i];
    v := TVK_LV_Variable.Create;
    arrItem.First;
    key := arrItem.CurrentKey;
    v.Expression := arrItem.s[key];
    Variables.AddOrSetValue(key, v);
    if arrItem.Contains(sMUTABLE) then
      v.mutable := arrItem.B[sMUTABLE];
  end;
end;

procedure TVK_LVPattern.AfterLoad(X: ISuperObject);
begin
  inherited;
  LoadVariables(X);
  LoadObjects(X);
end;

procedure TVK_LVPattern.BeforeSave(X: ISuperObject);
var
  i: Integer;
  arrItem: ISuperObject;
  arr: ISuperArray;
begin
  arr := X.A['Objects'];
  for i := 0 to Objects.Count-1 do
  begin
    arrItem := Objects[i].AsJSONObject;
    arr.Add(arrItem);
  end;
end;

constructor TVK_LVPattern.Create(const AListView: TListView);
begin
  Create;
  LV := AListView;
end;

destructor TVK_LVPattern.Destroy;
begin
  FreeAndNil(Variables);
  FreeAndNil(TextLayout);
  FreeAndNil(Columns);
  FreeAndNil(Objects);
  FreeAndNil(Parser);
  inherited;
end;

constructor TVK_LVPattern.Create;
begin
  inherited Create;
  TextLayout := TTextLayoutManager.DefaultTextLayout.Create;
  Columns := TVK_LVColumns.Create();
  Objects := TVK_LVObjects.Create();
  Variables := TVK_LV_Variables.Create([doOwnsValues]);
  Variables.Pattern := Self;
  Parser := TVK_ExpressionParser.Create('', ExpressionGetValueHandler);
end;

procedure TVK_LVPattern.CreateDrawables(const ALVItem: TListViewItem);
var
  i: integer;
  Drawable: TListItemDrawable;
begin
  for i := 0 to Objects.Count - 1 do
    Drawable := Objects[i].Drawable(ALVItem);
end;

procedure TVK_LVPattern.DoLayout(const ALVItem: TListViewItem);
var
  i: Integer;
  Drawable: TListItemDrawable;
begin
  FNowParsing.Setup(NIL, ALVItem, '');
  for i := 0 to Objects.Count - 1 do
  begin
    Drawable := Objects[i].Drawable(ALVItem);
    if Drawable<>NIL then
      Objects[i].SetupDrawable(Drawable);
  end;
  FNowParsing.Setup(nil, ALVItem, 'h');
  ALVItem.Height := Round( getvalue(ItemHeight) +0.5);
end;

// parser calls this method
// if faced with special words like AUTO, WIDTH, etc...
function TVK_LVPattern.ExpressionGetValueHandler(Sender: TObject;
  const AVarName: string): Single;
var
  liText: TListItemText;
  arr:TArray<string>;
  objName:string;
  fldName:string;
  obj:TVK_LVObject;
begin
  result := 0;
  if SameText(AVarName, 'itemwidth') then
  begin
    Result :=  GetClientWidth();
    exit;
  end
  // then
  else if Variables.ContainsKey(AVarName) then
  begin
    Result := Variables.ValueOf(AVarName);
  end
  else if SameText(AVarName, 'X') then
  begin
    Result := 0;
    if FNowParsing.Obj<>NIL then
      result := FNowParsing.Obj.FX;
  end
  else if SameText(AVarName, 'Y') then
  begin
    Result := 0;
    if FNowParsing.Obj<>NIL then
      result := FNowParsing.Obj.FY;
  end
  else if SameText(AVarName, 'W') then
  begin
    Result := 0;
    if FNowParsing.Obj<>NIL then
      result := FNowParsing.Obj.FW;
  end
  else if SameText(AVarName, 'H') then
  begin
    Result := 0;
    if FNowParsing.Obj<>NIL then
      result := FNowParsing.Obj.FH;
  end
  else if SameText(AVarName, 'remain') then
  begin
    if SameText(FNowParsing.Field, 'w') then
    begin
      Result :=  GetClientWidth();
      if FNowParsing.Obj<>NIL then
        result := Result - FNowParsing.Obj.FX;
      exit;
    end
    else if SameText(FNowParsing.Field, 'h') then
    begin
      if not (FNowParsing.Obj is TVK_LVTextObject) then
        raise Exception.Create('Can''t set AUTO height for non-textual element!');
      liText := FNowParsing.Obj.Drawable(FNowParsing.Item) as TListItemText;
      if liText<>NIL then
        Result := FNowParsing.Obj.CalculateHeight(liText);
    end;
  end
  else if SameText(AVarName, 'auto') then
  begin
    if SameText(FNowParsing.Field, 'w') then
    begin
      if not (FNowParsing.Obj is TVK_LVTextObject) then
        raise Exception.Create('Can''t set AUTO width for non-textual element!');
      liText := FNowParsing.Obj.Drawable(FNowParsing.Item) as TListItemText;
      if liText<>NIL then
        Result := FNowParsing.Obj.CalculateWidth(liText);
    end
    else if SameText(FNowParsing.Field, 'h') then
    begin
      if not (FNowParsing.Obj is TVK_LVTextObject) then
        raise Exception.Create('Can''t set AUTO height for non-textual element!');
      liText := FNowParsing.Obj.Drawable(FNowParsing.Item) as TListItemText;
      if liText<>NIL then
        Result := FNowParsing.Obj.CalculateHeight(liText);
    end;
  end
  else
  // parsing expressions like <NameOfSomeElement>.<Property>
  begin
    arr := AVarname.Split(['.']);
    if Length(arr)=2 then
    begin
      objName := arr[0];
      fldName := arr[1];
      obj := Objects. byName(objName);
      if obj=NIL then
        raise Exception.Create('Cant''t find object '+objName+'! ('+AVarName+')');
      if SameText(fldName,'x') or SameText(fldName,'left') then
      begin
        result := obj.X;
        exit;
      end
      else if SameText(fldName,'y') or SameText(fldName,'top') then
      begin
        result := obj.Y;
        exit;
      end
      else if SameText(fldName,'w') or SameText(fldName,'width') then
      begin
        result := obj.W;
        exit;
      end
      else if SameText(fldName,'h') or SameText(fldName,'height') then
      begin
        result := obj.H;
        exit;
      end
      else if SameText(fldName,'right') then
      begin
        result := obj.X+obj.W;
        exit;
      end
      else if SameText(fldName,'bottom') then
      begin
        result := obj.Y+obj.H;
        exit;
      end
    end;
  end;

  if Assigned(FOnGetValue) then
    FOnGetValue(Self, FNowParsing, AVarName, result);

end;

function TVK_LVPattern.GetValue(const AExpr: string): Single;
begin
  // ����� ������� - ���� ��� �����
  if TryStrToFloat(AExpr, result, localFS) then
    exit;
  // ������� ��������� ��������� ((
  Parser.Expression := AExpr;
  if Parser.Parse then
    result := Parser.Evaluate;
end;

function TVK_LVPattern.GetClientWidth: Single;
begin
  result := LV.width - LV.itemspaces.left - LV.itemspaces.right - lv.SideSpace*2;
  {$IFDEF MSWINDOWS}
  result := result - 30;
  {$ENDIF}
end;

{ TVK_LVImageObject }

function TVK_LVImageObject.CreateDrawable(const AItem: TListViewItem): TListItemDrawable;
begin
  result := TListItemImage.Create(AItem);
  Result.Name := Name;
end;

function TVK_LVImageObject.GetScalingMode: string;
begin
  case ScalingMode of
    TImageScalingMode.StretchWithAspect: result := sFIT;
    TImageScalingMode.Original: result := sORIG;
    TImageScalingMode.Stretch: result := sSTRETCH;
  end;
end;

procedure TVK_LVImageObject.SetScalingMode(const Value: string);
begin
  // 0,1,2 - Center, Leading, Trailing
  if AnsiSameText(value, sFIT)
  or AnsiSameText(value, '0') then
    ScalingMode := TImageScalingMode.StretchWithAspect
  else
  if AnsiSameText(value, sORIG)
  or AnsiSameText(Value, '1') then
    ScalingMode := TImageScalingMode.Original
  else
    ScalingMode := TImageScalingMode.Stretch;
end;

procedure TVK_LVImageObject.SetupDrawable(const ADrawable: TListItemDrawable);
var
  img: TListItemImage absolute ADrawable;
begin
  inherited SetupDrawable(ADrawable);
  img.ScalingMode := scalingMode;
end;

{ TVK_LV_ItemAdapter<TItm, TLst> }




procedure TVK_LV_ListAdapter<TDataItem>.ResetItem(
  const AListViewItem: TListViewItem);
begin
  lv.Adapter.ResetView(AListViewItem);
end;

procedure TVK_LV_ListAdapter<TDataItem>.ResetLayout;
var
  i:integer;
  ptn:TVK_LVPattern;
begin
  for i := 0 to lv.Items.Count-1 do
  begin
    ptn := SelectPattern(lv.Items[i]);
    if ptn<>nil then
    begin
      ptn.DoLayout(lv.Items[i]);
      AfterLayout(lv.Items[i]);
    end;
  end;
end;

procedure TVK_LV_ListAdapter<TDataItem>.ResetView;
var
  i: Integer;
  dataItem: TDataItem;
  lvItem: TListViewItem;
  allowed: Boolean;
begin
  if List=nil then
    exit;
  LV.BeginUpdate;
  try
    LV.Items.Clear;
    for i := 0 to List.Count - 1 do
    begin
      dataItem := List.items[i];
      allowed:=true;
      if Assigned(FOnFilter) then
        FOnFilter(dataItem, allowed);
      if allowed then
      begin
        lvItem := LV.Items.Add;
        lvitem.Purpose := TListItemPurpose.None;
      end;
    end;
  finally
    LV.EndUpdate;
  end;
end;

procedure TVK_LV_ListAdapter<TDataItem>.SetItemData(const AListViewItem: TListViewItem; const AData: TDataItem; const AName:string);
begin
  // for example
  // if AnsiSameText(AName,'ID') then
  //  AListViewItem.Data[AName] := AData.id;
end;


procedure TVK_LV_ListAdapter<TDataItem>.SetupDrawableContent(const AListViewItem: TListViewItem; const ADrawable: TListItemDrawable; const AData: TDataItem);
begin
 // for example

//  if SameText( ADrawable.Name, 'text') then
//  begin
//    (ADrawable as TListItemText).Text := AData.Text;
//  end
//  else if SameText( ADrawable.Name, 'detail') then
//  begin
//    (ADrawable as TListItemText).Text := AData.Detail;
//  end
end;


procedure TVK_LV_ListAdapter<TDataItem>.Clear;
begin
  inherited;
  list.Clear;
end;

function TVK_LV_ListAdapter<TDataItem>.GetDataByListItem(
  const AListViewItem: TListViewItem): TDataItem;
var
  i,index:integer;
begin
  // ���� ������ �������� ��������, �� �� �� ���������
  if AListViewItem=nil then
    result := nil
  else
  begin
    index := -1;
    for i := 0 to AListViewItem.Index do
      if (lv.Items[i] as TListViewItem).Purpose = TListItemPurpose.None then
        inc(index);

    if (Index<0)or(Index>=List.Count) then
      result := nil
    else
      result := List()[Index];
  end;
end;


function TVK_LV_BaseAdapter<TDataItem>.SelectPattern(
  const AListViewItem: TListViewItem): TVK_LVPattern;
begin
  result := NIL;
  if Patterns.Count>0 then
    result := Patterns[0];
end;

procedure TVK_LV_BaseAdapter<TDataItem>.SetupContent(
  const AListViewItem: TListViewItem);
begin

end;

procedure TVK_LV_BaseAdapter<TDataItem>.SetupListView;
begin
  if Patterns.Count>0 then
    Patterns[0].SetupListView(lv);
end;

procedure TVK_LV_ListAdapter<TDataItem>.SetupContent(const AListViewItem: TListViewItem);
var
  i: integer;
  Drawable: TListItemDrawable;
  index: Integer;
  DataItem: TDataItem;
  ptn : TVK_LVPattern;
  s:string;

begin
  if Patterns.Count=0 then
    exit;
  if List=nil then
    exit;

//  index := LV.Items.IndexOf(AListViewItem);
  index := LV.Items.ReindexAndFindItem(AListViewItem);

  if index = -1 then
    exit;
//    raise Exception.Create('WTF - Waltz Tango Foxtrot...');

  DataItem := GetDataByListItem(AListViewItem);
  // DataItem ����� ���� NULL !!!
  ptn := SelectPattern(AListViewItem);
  for i := 0 to ptn.Objects.Count - 1 do
  begin
    if sameText(ptn.Objects[i].Kind,'data') then
    begin
      SetItemData(AListViewItem, DataItem, ptn.Objects[i].Name);
    end
    else
    begin
      s:=ptn.Objects[i].Name;
      Drawable := ptn.Objects[i].Drawable(AListViewItem);
      SetupDrawableContent(AListViewItem, Drawable, DataItem);
    end;
  end;
end;



{ TNowParsing }

procedure TNowParsing.Clear;
begin
  Obj := nil;
  Item := nil;
  Field := '';
end;

procedure TNowParsing.Setup(const AObj: TVK_LVObject;
  const AItem: TListViewItem; const AField: string);
begin
  obj := AObj;
  item := AItem;
  Field := AField;
end;

{ TVK_LVObjects }

function TVK_LVObjects.ByName(const AName: string): TVK_LVObject;
var
  index:integer;
begin
  result := NIL;
  index := IndexOfName(AName);
  if index<>-1 then
    Result := Items[index];
end;

function TVK_LVObjects.IndexOfName(const AName: string): Integer;
var
  i: Integer;
begin
  result := -1;
  for i := 0 to Count-1 do
  begin
    if SameText(Items[i].Name,AName) then
    begin
      Result := i;
      exit;
    end;
  end;
end;

procedure TVK_LVObjects.ResetPlaces;
var
  i: Integer;
begin
  for i := 0 to Count-1 do
    Items[i].ResetPlace;
end;

{ TVK_LVRectObject }

function TVK_LVRectObject.CreateDrawable(
  const AItem: TListViewItem): TListItemDrawable;
begin
  result := TVKListItemRect.Create(AItem);
  Result.Name := Name;
end;

procedure TVK_LVRectObject.SetupDrawable(const ADrawable: TListItemDrawable);
var
  r: TVKListItemRect absolute ADrawable;
begin
  inherited SetupDrawable(ADrawable);
  if Color<>'' then
    r.Color := stringtoalphacolor(Color);
  if BorderColor<>'' then
    r.BorderColor := stringtoalphacolor(BorderColor);
  r.LineWidth := LineWidth;
  if SelectedColor<>'' then
    r.SelectedColor := stringtoalphacolor(SelectedColor);
  if SelectedBorderColor<>'' then
    r.SelectedBorderColor := stringtoalphacolor(SelectedBorderColor);
  r.XRadius := XRadius;
  r.YRadius := YRadius;
  r.Shadow := Shadow;
end;

{ TVK_LV_Adapter<TDataItem> }

function TVK_LV_BaseAdapter<TDataItem>.AddPatternFromFile(
  const Filename: string): TVK_LVPattern;
begin
  result := TVK_LVPattern.CreateFromFile(Filename);
  result.LV := LV;
  Patterns.Add(result);
end;

function TVK_LV_BaseAdapter<TDataItem>.AddPatternFromJSON(
  const JSON: string): TVK_LVPattern;
begin
  result := TVK_LVPattern.CreateFromJSON(JSON);
  result.LV := LV;
  Patterns.Add(result);
end;

procedure TVK_LV_BaseAdapter<TDataItem>.AfterLayout(
  const AListViewItem: TListViewItem);
begin
  if Assigned(OnAfterLayout) then
    OnAfterLayout(Self, AListViewItem);
end;

procedure TVK_LV_BaseAdapter<TDataItem>.Clear;
begin
  LV.Items.Clear;
end;

constructor TVK_LV_BaseAdapter<TDataItem>.Create(const AListView: TListView;
  const JSON: string);
begin
  Create(AListView);
  AddPatternFromJSON(JSON);
  SetupListView();
end;

constructor TVK_LV_BaseAdapter<TDataItem>.Create(const AListView: TListView);
begin
  LV := AListView;
  LV.OnUpdatingObjects := OnUpdatingObjects;
  Patterns:= TVK_LVPatterns.Create;
end;

destructor TVK_LV_BaseAdapter<TDataItem>.Destroy;
begin
  FreeAndNil(Patterns);
  inherited;
end;

procedure TVK_LV_BaseAdapter<TDataItem>.DoLayout(
  const AListViewItem: TListViewItem);
var
  ptn : TVK_LVPattern;
begin
  ptn := SelectPattern(AListViewItem);
  ptn.DoLayout(AListViewItem);
end;


procedure TVK_LV_BaseAdapter<TDataItem>.OnUpdatingObjects(const Sender: TObject;
  const AItem: TListViewItem; var AHandled: Boolean);
begin
  SetupContent(AItem);
  DoLayout(AItem);
  AfterLayout(AItem);
  AHandled := true;
end;

procedure TVK_LV_BaseAdapter<TDataItem>.ResetPlaces;
var
  i: Integer;
begin
  for i := 0 to Patterns.Count-1 do
  begin
    Patterns[i].Objects.ResetPlaces;
  end;
end;

procedure TVK_LV_BaseAdapter<TDataItem>.ResetView;
begin
  // ��� ���������� LV.Add ������� ������� �����
end;

{ TVK_LVPatterns }

function TVK_LVPatterns.AddFromFile(const Filename: string): TVK_LVPattern;
begin
  result := TVK_LVPattern.CreateFromFile(Filename);
  Add(result);
end;

function TVK_LVPatterns.AddFromJSON(const JSON: string): TVK_LVPattern;
begin
  result := TVK_LVPattern.CreateFromJSON(JSON);
  Add(result);
end;

{ TVK_LVAccessoryObject }

function TVK_LVAccessoryObject.CreateDrawable(
  const AItem: TListViewItem): TListItemDrawable;
begin
  result := TListItemAccessory.Create(AItem);
  Result.Name := Name;
end;

function TVK_LVAccessoryObject.Get_AccType: string;
begin
  case acctype of
    TAccessoryType.More: result := 'More';
    TAccessoryType.Checkmark: result := 'Check';
    TAccessoryType.Detail: result := 'Detail'
  end;
end;

procedure TVK_LVAccessoryObject.SetupDrawable(
  const ADrawable: TListItemDrawable);
begin
  inherited SetupDrawable(ADrawable);

end;

procedure TVK_LVAccessoryObject.Set_AccType(const Value: string);
begin
  if SameText(Value, 'More') then
    Acctype := TAccessoryType.More
  else
  if SameText(Value, 'Check') or  SameText(Value, 'Checkmark') then
    Acctype := TAccessoryType.checkmark
  else
    Acctype := TAccessoryType.detail;
end;

{ TVK_LV_DataList<T> }

procedure TVK_LV_DataList<T>.AfterLoad(X: ISuperObject);
begin
  ArrayToList<T>(_Items, items);
end;

constructor TVK_LV_DataList<T>.Create;
begin
  inherited;
  Items := TObjectList<T>.Create(True);
end;

destructor TVK_LV_DataList<T>.Destroy;
begin
  FreeAndNil(Items);
  inherited;
end;

{ TVK_LV_Variables }

function TVK_LV_Variables.ValueOf(const VarName: string): single;
var
  v:TVK_LV_Variable;
begin
  v := Items[VarName]; // Exception when not exists
  if (v.mutable) or (not v.evaluated) then
  begin
    v.value := Pattern.GetValue(v.Expression);
    v.evaluated := true;
  end;
  result := v.value;
end;

{ TVK_LVEllipseObject }

function TVK_LVEllipseObject.CreateDrawable(
  const AItem: TListViewItem): TListItemDrawable;
begin
  result := TVKListItemEllipse.Create(AItem);
  Result.Name := Name;
end;

procedure TVK_LVEllipseObject.SetupDrawable(const ADrawable: TListItemDrawable);
var
  r: TVKListItemEllipse absolute ADrawable;
begin
  inherited SetupDrawable(ADrawable);
  if Color<>'' then
    r.Color := stringtoalphacolor(Color);
  if BorderColor<>'' then
    r.BorderColor := stringtoalphacolor(BorderColor);
  r.LineWidth := LineWidth;
  r.Circle := Circle;
end;

initialization
  localFS := TFormatSettings.Create();
  localFS.DecimalSeparator := '.';

finalization

end.

